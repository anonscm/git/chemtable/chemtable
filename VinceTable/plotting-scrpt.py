import h5py
import numpy as np
import matplotlib.pyplot as plt
import sys

def ReadAIh5(file,Var2,Var3,Var4,Data):
  comp=float(file.split('/')[-1].split('.hdf5')[0].split('.h5')[0].split('-')[1])
  #if ("Yfuel" in file):
  #	type_comp='Y'
  #elif("phi" in file):
  #  type_comp='phi'
  h5f=h5py.File(file)
  X=[]

  cols1=list(h5f.keys())
  col1=('Var2-%5.3e' %Var2)
  if (col1 not in cols1):
  	print ('Temperature value %2.2f is not in keys range try with one of these ' %(Var2))
  	print (cols1)
  	sys.exit(1)

  cols2=list(h5f[col1].keys())
  col2=('Var3-%2.2f' %Var3)
  if (col2 not in cols2):
  	print ('column %s is not in columns try with one of these ' %(Var3))
  	print (cols2)
  	sys.exit(1)

  cols3=list(h5f[col1][col2].keys())
  col3=('Var4-%2.3f' %Var4)
  #print('Var4-%2.3f' %Var4)
  if (col3 not in cols3):
  	print ('column %s is not in columns try with one of these ' %(Var4))
  	print (cols3)
  	sys.exit(1)

  cols4=list(h5f[col1][col2][col3].keys())
  col4=(Data)
  if (col4 not in cols4):
  	print ('column %s is not in columns try with one of these ' %(Data))
  	print (cols4)
  	sys.exit(1)
 
  X=(h5f[col1][col2][col3][col4][...])

  return X

#file='/Users/robin/Documents/RECHERCHES/ETUDE/CANTERA/CHEMTABLE-GEN/CAS3-EGLANTINE/TEST/calcul/AI/Var1-500000.000.hdf5'
#file='/Users/robin/Documents/RECHERCHES/ETUDE/CANTERA/CHEMTABLE-GEN/CAS6-H2-EGLANTINE/TEST/calcul/AI/Var1-500000.000.hdf5'
file='/Users/robin/Documents/RECHERCHES/ETUDE/CANTERA/CHEMTABLE-GEN/CAS-5-6-DEPART-EGLANTINE/CAS6-H2-EGLANTINE/RESU/calcul/AI/Var1-500000.000.hdf5'

Var2=1200
Var3=0.0

#Var4=0.1
#Data1='Temperature'
#Data2='Time'
#H2=ReadAIh5(file,Var2,Var3,Var4,Data1)
#Time=ReadAIh5(file,Var2,Var3,Var4,Data2)
#plt.plot(Time,H2)
#plt.show()
Delaytime= []
invDelaytime= []
Mixturefrac= []
nvar4 = 21
Var4=0.0
for ivar4 in range(nvar4):
  delay = ReadAIh5(file,Var2,Var3,Var4,'Ignitiontime')
  invdelay = 1/delay
  Delaytime.append(delay)
  invDelaytime.append(invdelay)
  Mixturefrac.append(Var4)
  Var4+=0.05
#plt.plot(Mixturefrac,invDelaytime)
plt.plot(Mixturefrac,Delaytime)
plt.show()
