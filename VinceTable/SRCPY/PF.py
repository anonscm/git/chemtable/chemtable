import cantera as ct
import numpy as np
from SRCPY.IO import message,Write_h5_PF
from SRCPY.utils import diff

def MassFrac2phi(gas,fuel_species,YF,N2=True):
    Gamma       = 3.76
    try:
      phi_s   = gas.n_atoms(fuel_species,'C') + 0.25*gas.n_atoms(fuel_species,'H')
    except:
      phi_s   = 0.25*gas.n_atoms(fuel_species,'H')
    if(not N2):
      phi=phi*YF/(1.0-YF)
    else:
      in2=gas.species_index('N2')
      io2=gas.species_index('O2')
      Gamma=Gamma*gas.molecular_weights[in2]/gas.molecular_weights[io2]
      phi=YF*phi_s*(1+Gamma)/(1.0-YF)
    return phi


def ChemInit(chem,fuel_species,N2=True):
    gas         = ct.Solution(chem)
    Gamma       = 3.76
    try:
      stoich_O2   = gas.n_atoms(fuel_species,'C') + 0.25*gas.n_atoms(fuel_species,'H')
    except:
      stoich_O2   = 0.25*gas.n_atoms(fuel_species,'H')

    indexes     = { fuel_species : gas.species_index(fuel_species),
                    "O2"         : gas.species_index('O2'),
                    "N2"         : gas.species_index('N2')}                 
    
    xo2=stoich_O2
    
    if (N2):
      xn2=stoich_O2*Gamma
    else:
      xn2=0.0

    moles       = { fuel_species : 0.0,
                    "O2"         : xo2,
                    "N2"         : xn2}

    n_species   = gas.n_species
    X0          = np.zeros(n_species,'d')
    X0[indexes["O2"]]=moles['O2']
    X0[indexes["N2"]]=moles['N2']
    return gas,indexes,moles,X0


def FlameInit(T0, P, X0,phi,alpha,gas,indexes,fuel_species,cond='hot'):
    X0[indexes[fuel_species]] = phi
    gas.TPX                   = T0, P, X0
    if (cond.lower()=='hot'):
        cd='HP'
    elif(cond.lower()=='cold'):
        cd='TP'

    if (alpha>0):
        Y                = 0.0*np.copy(gas.Y)
        Yu               = np.copy(gas.Y)
        gas.equilibrate(cd, solver='vcs', rtol=1e-09, maxsteps=1000, maxiter=100, loglevel=0)
        Yb               = np.copy(gas.Y)
        
        for k in range(gas.n_species):
            s=gas.species_names[k]
            if (s in ["O2",'N2',fuel_species]):
                i=indexes[s]
                Y[i]=Yu[i]*(1-alpha)+alpha*Yb[i]
            else:
                Y[k]=alpha*Yb[k]
        if (cd=='TP'):
            gas.TPY=gas.T,gas.P,Y
        elif(cd=='HP'):
            H,P=gas.HP
            gas.HPY=H,P,Y
            T=gas.T
    
    Xu=np.copy(gas.X)
    return gas,Xu

def FlameSolve(gas,width,tol_ss=[1.0e-8, 1.0e-16],tol_ts=[1.0e-8, 1.0e-16],ratio=3,
    slope=0.03, curve=0.06,loglevel=1):  
    f = ct.FreeFlame(gas, width=width)
    f.inlet.X = gas.X
    f.inlet.T = gas.T
    f.energy_enabled = True
    f.flame.set_steady_tolerances(default=tol_ss)
    f.flame.set_transient_tolerances(default=tol_ts)
    # f.set_refine_criteria(ratio=ratio, slope=slope, curve=curve) 
    f.transport_model = 'Mix'
    f.solve(loglevel=loglevel, auto=True)
    return f

def PostFlame(flame,gas,Xu,phi,alpha,fuel_species,condition):
    dict_data={}
    dict_data['location (m)']        = flame.grid
    dict_data['velocity (m per s)']      = flame.u
    dict_data["density (kg per m3)"]     = flame.density
    dict_data['Temperature (K)']     = flame.T
    dict_data["Heat release (W per m3)"] =flame.heat_release_rate
    for ispecies in range(gas.n_species):
        name=gas.species_names[ispecies]
        dict_data[name]=flame.Y[ispecies,:]

    ifuel=gas.species_index(fuel_species)
    dict_attr={}
    dict_attr['fuel']        = fuel_species
    dict_attr['phi']         = phi
    dict_attr['dilution (%)']= alpha*100
    dict_attr['dil_type']    = condition
    dict_attr['T_init (K)']  = flame.T[0]
    dict_attr['Y_F']         = flame.Y[ifuel,0]
    dict_attr['P (Pa)']      = flame.P
    dict_attr['SL (m per s)']    = flame.u[0]
    dict_attr['Tb (K)']      = flame.T[-1]
    dict_attr['Length (m)']  = flame.grid[-1]

    gas.TPX = flame.T[0], flame.P, Xu
    gas.equilibrate('HP', solver='vcs', rtol=1e-09, maxsteps=1000, maxiter=100, loglevel=0)
    dict_attr["Teq (K)"] = gas.T
    for ispecies in range(gas.n_species):
        name='Y-Eq_'+gas.species_names[ispecies]
        dict_attr[name]=gas.Y[ispecies]

    # compute progress variable
    dict_data['C']               =(flame.T-np.amin(flame.T))/(dict_attr["Teq (K)"]-np.amin(flame.T))
    dict_data["GradientC (1 per m)"]=diff(dict_data["location (m)"],dict_data["C"])
    dict_attr["thickness (m)"]  =1.0/np.amax(abs(dict_data["GradientC (1 per m)"]))

    return dict_attr,dict_data


def GetPhi(gas,indexes):
    stoich_O2   = gas.n_atoms(fuel_species,'C') + 0.25*gas.n_atoms(fuel_species,'H')
    phis=stoich_O2*gas.molecular_weights[indexes["O2"]]/gas.molecular_weights[indexes[fuel_species]]
    phi= phis*gas.Y[indexes[fuel_species]]/gas.Y[indexes["O2"]]
    return phi 


##########################
def PFCompute(chem,fuel_species,N2_present,comp_mode,condition,directory,comp,T,P,dil,width,\
logfile="screen",loglevel=0,tol_ss=[1.0e-8, 1.0e-16],tol_ts=[1.0e-8, 1.0e-16],\
ratio=3,slope=0.03, curve=0.06):
  msg='Initializing chemistry'
  message(msg,logfile)
  gas,indexes,moles,X0= ChemInit(chem,fuel_species)
  Ncomp=len(comp)
  NT   =len(T)
  NP   =len(P)
  Ndil =len(dil)
  ext="hdf5"
  if comp_mode == 'phi':
    filefmt='%s/phi-%%2.3f.%s' %(directory,ext)
    Phi=comp
  elif comp_mode == 'y':
    filefmt='%s/Yfuel-%%2.3f.%s' %(directory,ext)
    Phi=MassFrac2phi(gas,fuel_species,comp,N2_present)

  NoSol =[]
  DA    =[]
  DD    =[]
  names=gas.species_names
  for icomp in range(Ncomp):
    fileName=filefmt %comp[icomp]
    for iT in range(NT):
      for iP in range(NP):
        for idil in range(Ndil):
          phi   = Phi[icomp]
          T0    = T[iT]
          pres  = P[iP]
          alpha = dil[idil]
          msg='Processing case : P=%2.4e, phi=%1.2f , T=%2.2f , dil=%1.2f%%' %(pres,phi,T0,alpha*1e2)
          message(msg,logfile)
          gas,Xu    = FlameInit(T0, pres,X0,phi,alpha,gas,indexes,fuel_species,condition)
          try:
            flame = FlameSolve(gas,width,loglevel=loglevel,tol_ss=tol_ss,\
              tol_ts=tol_ts,ratio=ratio,slope=slope, curve=curve)
            dict_attr,dict_data=PostFlame(flame,gas,Xu,phi,alpha,fuel_species,condition)
            DA.append(dict_attr)
            DD.append(dict_data)
            msg='SL (m/s) = %2.3f , Tb (K) = %2.2f' %(flame.u[0],flame.T[-1])
            message(msg,logfile)
          except:
            point="Phi= %1.2f, T= %2.2f, dil= %1.2f, Pres= %2.4e, cond= %s" %(phi,T0,alpha*1e2,pres,condition)
            NoSol.append(point)
    Write_h5_PF(fileName,names,DA,DD,logfile)
  return NoSol