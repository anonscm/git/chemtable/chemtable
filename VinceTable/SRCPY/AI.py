from SRCPY.IO import Write_h5_AI,message,Write_h5_AI_vince
from math import log10, pow
import cantera as ct
import numpy as nvar2
import sys
import os
def GetCompResidual(T,Teq):
    res=abs(T-Teq)/Teq
    return res

# def XDil(gas,fuel_species,alpha,cd='HP'):
#  # Y                = 0.0*nvar2.copy(gas.Y)  
#  # Yu               = nvar2.copy(gas.Y)
#   gas.equilibrate(cd, solver='vcs', rtol=1e-09, maxsteps=1000, maxiter=100, loglevel=0)
#   #Yb               = nvar2.copy(gas.Y)
#   Teq              =gas.T
# #  for k in range(gas.n_species):
# #    s=gas.species_names[k]
# #    if (s in ["O2",'N2',fuel_species]):
# #      i=gas.species_index(s)
# #      Y[i]=Yu[i]*(1.0-alpha)+alpha*Yb[i]
# #    else:
# #      Y[k]=alpha*Yb[k]
# #  if (cd=='TP'):
# #    gas.TPY=gas.T,gas.Var1,Y
# #  elif(cd=='HP'):
# #    H,Var1=gas.HP
# #    gas.HPY=H,Var1,Y
#  # return gas,Teq
#   return Teq



def compute(scheme,fuel,Var1,Var2,Var3,Var4,nt,dt,comp_mode,logfile,res_max=1e-3,Ite_max=1e6,condition="HP"):
    msg="Processing case with : Var1 = %14.8e , Var2= %2.2f , Var3=%2.3f , Var4=%2.3f" %(Var1,Var2,Var3,Var4)
    message(msg,logfile)

    cti = ct.Solution(scheme)
    scheme2='%s_NR.cti' %(scheme.split('.cti')[0])
    gas_a = ct.Solution(scheme2)
    gas_b = ct.Solution(scheme2)
    gasequil=ct.Solution(scheme)
    ##########
    # vincent : 
    # je fixe le nombre d'iteration à 10 000 et je considère que
    # "time" donné par l'utilisateur est le temps max de simu (ça peut être moins)
    # et je considère que
    # "dt"   donné par l'utilisateur est le pas de temps min (ça peut être plus)
    # je calcul donc d'abord le dt reel et le temps de simu reel
    # et le nombre de calculs necessaires.
    # Le pas de temps est multiplié par 10 entre chaque calcul
    # j'utilise la méthode de vérification de la convergence d'Aimad. 
    ##########
    ntfix=Ite_max
    dtmax=dt*nt/ntfix
    ncalcul=int(log10(dtmax/dt)+1)
    try:
     for mm in range(ncalcul):
        Time          = []
        Temperature   = []
        Mass_Frac     = []
        Tmax          = 0.0
        derive        = 0.0
        Delaytime     = 0.0
        InDelaytime   = 0.0
        dtcalcul=dt*pow(10,mm)   

# vince, on determine la composition intiale à partir de plusieurs réacteurs (a faire ailleurs pour otpimiser le temps de calcul)
# la variable var4 est une fraction de mléange
        frac = Var4
#        gas_a.TPX = 320.0, ct.one_atm, 'CH4:0.33, O2:0.15, N2:0.52'
#        gas_b.TPX = 1350.0, ct.one_atm, 'O2:0.12, N2:0.73, H2O:0.15'
#
        gas_a.TPX = 1050.0, ct.one_atm, 'H2:2.5, O2:1.0, N2:3.76'
        gas_b.TPX = 1100.0, ct.one_atm, 'H2:0.25, O2:1.0, N2:3.76'

#        gas_a.TPX = 800.0, ct.one_atm, 'H2:2.0, O2:1.0, N2:3.76'
#        gas_b.TPX = 800.0, ct.one_atm, 'H2:0.25, O2:1.0, N2:3.76' 


# premier TEST avec nheptane : à faire.
#        gas_a.TPX = 1000.0, ct.one_atm, 'NC7H16:0.1818, O2:1.0, N2:3.76'
#        gas_b.TPX = 1000.0, ct.one_atm, 'NC7H16:0.04545, O2:1.0, N2:3.76'
#        gas_a.TPX = 1400.0, ct.one_atm, 'NC7H16:0.15, O2:1.0, N2:3.76'
#        gas_b.TPX = 1400.0, ct.one_atm, 'NC7H16:0.07, O2:1.0, N2:3.76'

        res_a = ct.Reservoir(gas_a)
        res_b = ct.Reservoir(gas_b)
        downstream = ct.Reservoir(gas_b)
        mixer = ct.IdealGasReactor(gas_b)
        mfc1 = ct.MassFlowController(res_a, mixer, mdot = frac)
        mfc2 = ct.MassFlowController(res_b, mixer, mdot = 1.0-frac)
        outlet = ct.Valve(mixer, downstream, K = 10.0)
        simix = ct.ReactorNet([mixer])
        t = 0.0
        for n in range(30):
            tres = mixer.mass/(mfc1.mdot(t) + mfc2.mdot(t))
            t += 0.5*tres
            simix.advance(t)
 
        cti.TPX = mixer.T, ct.one_atm, mixer.thermo.X
        gasequil.TPX = mixer.T, ct.one_atm, mixer.thermo.X
        #Teq=XDil(gasequil,fuel,Var3,cd=condition)
        gasequil.equilibrate('HP', solver='vcs', rtol=1e-09, maxsteps=1000, maxiter=100, loglevel=0)
        Teq              =gasequil.T

        r       = ct.ConstPressureReactor(cti)
        sim     = ct.ReactorNet([r])
        time    = 0.0
        for n in range(ntfix):
            time += dtcalcul
            sim.advance(time)
            Time.append(time)
            Temperature.append(r.T)
            Mass_Frac.append(r.Y)   # on obtient fraction massique par especes
            res=GetCompResidual(r.T,Teq)
            if (res<=res_max):
                msg='Convergence obtained within dt = %g s' %dtcalcul
                msg2='Convergence obtained at iteration = %g' %n
                message(msg,logfile)
                message(msg2,logfile)
                break
        if (res<=res_max):
           break
        if ( ((n+1)==ntfix) & (res>res_max)) :
            msg='Case non converged.'
            message(msg,logfile)

     Time=nvar2.array(Time)
     Temperature=nvar2.array(Temperature)
     Mass_Frac=nvar2.array(Mass_Frac)
     fmt="Resiual = %14.8e ,Steps taken= %%0%gd, Time=%14.8e s, Temperature = %2.2f , Teq=%2.2f" %(res,len(str(nt)),time,r.T,Teq)
     msg=fmt %(n+1)
     message(msg,logfile)
     # compute derivative
     if ((nvar2.amax(Temperature)-nvar2.amin(Temperature))>=50):
        derive=(Temperature[1:len(Temperature)]-Temperature[0:len(Temperature)-1])/dt
        Delaytime=Time[nvar2.argmax(derive)]
     flag=0
    except:
     msg="Failure happend when computing case with : Var1 = %14.8e , Var2= %2.2f, Var4(%s)=%2.3f " %(Var1,Var2,comp_mode.upper(),Var4)
     message(msg,logfile)
     Time          = None
     Temperature   = None
     Mass_Frac     = None
     flag=-1

    return Time,Temperature,Mass_Frac,Delaytime,flag
        

def AICompute(scheme,fuel,Var1,Var2,Var3,Var4,dt,Time,Dir,comp_mode,\
            res_max=1e-3,Ite_max=1e6,logfile="screen",condition='HP'):
    cti = ct.Solution(scheme)
    nt  = int(Time/dt) 
    ext="hdf5"
    if comp_mode == 'phi':
        filefmt='%s/Var1-%%2.3f.%s' %(Dir,ext)
    elif comp_mode == 'y':
        filefmt='%s/Yfuel-%%2.3f.%s' %(Dir,ext)
    if (condition=="HP"):
      dil_type="hot"
    elif (condition=="TP"):
      dil_type="cold"

    Unsolved=[]
    nvar1 = len(Var1)
    nvar2 = len(Var2)
    nvar3 = len(Var3)
    nvar4 = len(Var4)
    for ivar1 in range(nvar1):
      fileName=filefmt %Var1[ivar1]     
      Dicts    =[]
      for ivar2 in range(nvar2):
       for ivar3 in range(nvar3):
        for ivar4 in range(nvar4):
          Time,Temperature,Mass_Frac,Delaytime,flag=\
          compute(scheme,fuel,Var1[ivar1],Var2[ivar2],Var3[ivar3],Var4[ivar4],nt,dt,comp_mode,\
                  logfile,res_max=res_max,Ite_max=Ite_max,condition=condition)
          if (flag==0):
            Dict        ={}
            Dict['fuel']=fuel
            Dict['Dil_type']=dil_type
            Dict['Var2']=Var2[ivar2]
            Dict['Var3']=Var3[ivar3]
            Dict['Var4']=Var4[ivar4]
            Dict['Time']=Time
            Dict['Temperature']=Temperature
            Dict['Y']   =Mass_Frac
            Dict['tign']=Delaytime
            Dicts.append(Dict)
          else:
            D     ={}
            D['Var1']=Var1[ivar1]
            D['Var2']=Var2[ivar2]
            D['Var3']=Var3[ivar3]
            D['Var4']=Var4[ivar4]
            Unsolved.append(D)

      #Write_h5_AI_vince(fileName,cti.species_names,Dicts,logfile)
      Write_h5_AI(fileName,cti.species_names,Dicts,logfile)
