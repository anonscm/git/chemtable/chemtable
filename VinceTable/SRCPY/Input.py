from SRCPY.utils import ConfigSectionMap
import cantera as ct
import numpy as np
import getpass
import sys
import os


class Input():
  def __init__(self, file_ini):
    self.Ini = file_ini
    dict_discr=self.ConfigSectionMap(self.Ini,"Discretization")
    dict_therm=self.ConfigSectionMap(self.Ini,"Thermochemistry")
    dict_opera=self.ConfigSectionMap(self.Ini,"Operation")
    dict_AIprm=self.ConfigSectionMap(self.Ini,"AI_params")
    dict_PFprm=self.ConfigSectionMap(self.Ini,"PF_params")
    dict_outpt=self.ConfigSectionMap(self.Ini,"Output")

    self.N_Var1      =dict_discr['variable 1 points num.']
    self.N_Var2      =dict_discr['variable 2 points num.']
    self.N_Var3      =dict_discr['variable 3 points num.']
    self.N_Var4      =dict_discr["variable 4 points num."]

    self.Disc_mode   =dict_discr['mode']
    self.Var1_file   =dict_discr['variable 1 points file']
    self.Var2_file   =dict_discr['variable 2 points file']
    self.Var3_file   =dict_discr['variable 3 points file']
    self.Var4_file   =dict_discr['variable 4 points file']
    self.Comp_mode   =dict_discr['composition mode']

    self.Scheme      =dict_therm['scheme']
    self.Var1_range  =dict_therm['variable 1 range']
    self.Var2_range  =dict_therm['variable 2 range']
    self.Var3_range  =dict_therm['variable 3 range']
    self.Var4_range  =dict_therm['variable 4 range']
    self.BGTemp      =dict_therm['bg temperature']

    self.Fuel        =dict_therm['fuel']

    self.Operation   =dict_opera['mode']

    self.AI_time     =dict_AIprm['timemax']
    self.AI_dt       =dict_AIprm['dtmin']
    self.Max_Res     =dict_AIprm['maximum residual']
    self.Max_Ite     =dict_AIprm['iters']


    self.isN2        =dict_PFprm['consider nitrogen']
    self.length      =dict_PFprm['domain length']
    self.loglevel    =dict_PFprm['flame solver verbosity']
    self.SS          =dict_PFprm['steady-state tolerance']
    self.TS          =dict_PFprm['time stepping tolerance']
    self.ratio       =dict_PFprm['min. spacing ratio']
    self.slope       =dict_PFprm['max. spacing difference']
    self.curve       =dict_PFprm['max. slope difference']


    self.Out_dir     =dict_outpt['root directory']
    self.LogFile     =dict_outpt['output log']

    self.Var1        = None
    self.Var2        = None
    self.Var3        = None

    self.InputCheck()
    self.GetRanges()

  def ConfigSectionMap(self,Ini,keyword):
      Dict=ConfigSectionMap(Ini,keyword)
      Dict=self.clean_dict(Dict)
      return Dict

  def clean_dict(self,Dict):
      for k in Dict.keys():
        Dict[k]=Dict[k].split(';')[0].strip()
      return Dict

  def InputError(self,message):
    print ("*"*75)
    print ("*"*5,'INPUT ERROR :  %s ' %message)
    print ("*"*5, 'THE PROGRAM WILL HALT ! PLEASE RE-CHECK YOUR INPUT AND TRY AGAIN.')
    print ("*"*75)
    sys.exit(1)
  
  def range2array(self,string):
      array=np.array(string.split('[')[-1].split(']')[0].split(','),dtype=np.float)
      return array

  def checkvalue(self,Type,value,Name):
    try:
      if(Type=="integer"):
        value=int(value)
      elif(Type=="float"):
        value=float(value)
      return value
    except:
      if(Type=="integer"):
        self.InputError("%s should be an integer, you entered %s" %(Name,value))
      elif(Type=="float"):
        self.InputError("%s should be a float, you entered %s" %(Name,value))

  def CheckRange(self,RANGE,Name):
   try:
    RANGE=self.range2array(RANGE)
    return RANGE
   except:
    self.InputError("Poblem occured while parsing %s range, you entered %s" (NAME,RANGE))

  def checkChem(self):
    if (not os.path.isfile(self.Scheme)):
        self.InputError("mechanism file %s doesn't exist." %self.Scheme)
    try:
      gas=ct.Solution(self.Scheme)
      iF=gas.species_index(self.Fuel)
    except:
      self.InputError("Fuel species %s not found in the mechanism file. Check its spelling and case." %self.Fuel)

  def CheckFile(self,File,Name):
   if (not os.path.isfile(File)):
     self.InputError("%s file %s doesn't exist." %(Name,File))
  
  def checkstr(self,value,val1,val2,Name):
    if (value.lower() ==val1):
      value=val1
      return value
    elif (value.lower() ==val2):
      value=val2
      return value
    else:
      self.InputError("Unknown %s : %s" %(Name,value))
  
  def GetPoints(self,file):
    try:
      f=open(file)
      lines=f.readlines()
      f.close()
      N=len(lines)-1
      X=np.zeros(N)
      for i in range(N):
        X[i]=float(lines[i+1])
      X=np.sort(X)
      return X
    except:
      self.InputError("Problem occured while reading file %s" %file)

  def GetRanges(self): 
    if (self.Disc_mode=="auto"):
      self.Var1=np.linspace(self.Var1_range[0],self.Var1_range[1],self.N_Var1)
      self.Var2=np.linspace(self.Var2_range[0],self.Var2_range[1],self.N_Var2)
      self.Var3=np.linspace(self.Var3_range[0],self.Var3_range[1],self.N_Var3)
      self.Var4=np.linspace(self.Var4_range[0],self.Var4_range[1],self.N_Var4)
    elif(self.Disc_mode=="udf"):
      self.P     = self.GetPoints(self.Var2_file)
      self.T     = self.GetPoints(self.Var3_file)
      self.Comp   = self.GetPoints(self.Var1_file)
      self.dil   = self.GetPoints(self.Var4_file)
      self.N_Var2   = len(self.P)
      self.N_Var3   = len(self.T)
      self.N_Var1 = len(self.Comp)
      self.N_Var4 = len(self.dil)
      self.Var2_range=[np.amin(self.P),np.amax(self.P)]
      self.Var3_range=[np.amin(self.T),np.amax(self.T)]
      self.Var1_range=[np.amin(self.Comp),np.amax(self.Comp)]
      self.Var4_range=[np.amin(self.dil),np.amax(self.dil)]
    
  def InputCheck(self):
      # Check Chemistry
      self.checkChem()
      # Check Discretization mode
      self.Disc_mode=self.checkstr(self.Disc_mode,"auto","udf","discretization mode")
      # Check composition mode
      self.Comp_mode=self.checkstr(self.Comp_mode,"phi","y","composition mode")
      self.BGTemp   =self.checkstr(self.BGTemp,"hot","cold","BG Temperature")

      # Check discretization files (UDF discretization mode case)
      if (self.Disc_mode=='udf'):
        self.CheckFile(self.Var2_file,"Pressure points")
        self.CheckFile(self.Var3_file,"Temperature  points")
        self.CheckFile(self.Var1_file,"Composition points")
        self.CheckFile(self.Var4_file,"BG Dilution points")
      # Check Ranges  (AUTO discretization mode case)
      elif (self.Disc_mode=='auto'):
        self.N_Var2   =self.checkvalue("integer",self.N_Var2,"Pressure points")
        self.N_Var3   =self.checkvalue("integer",self.N_Var3,"Temperature points Num")
        self.N_Var1   =self.checkvalue("integer",self.N_Var1,"Composotion points Num")
        self.N_Var4   =self.checkvalue("integer",self.N_Var4,"BG Dilution points Num")
        self.Var2_range=self.CheckRange(self.Var2_range,"Pressure")
        self.Var3_range=self.CheckRange(self.Var3_range,"Temperature")
        self.Var1_range=self.CheckRange(self.Var1_range,"Composition")
        self.Var4_range=self.CheckRange(self.Var4_range,"BG Dilution")
      # Check Operation type
      self.Operation=self.checkstr(self.Operation,"ai","pf","Operation")
      if (self.Operation=="ai"):
        # Check Auto ignition parameters
        self.AI_time=self.checkvalue("float",self.AI_time,"Simulation final time")
        self.AI_dt=self.checkvalue("float",self.AI_dt,"Advancement time step")
        self.Max_Res=self.checkvalue("float",self.Max_Res,"Maximum residual")
        self.Max_Ite=self.checkvalue("integer",self.Max_Ite,"Maximum additional iterations")

      elif (self.Operation=="pf"):
        self.isN2     =self.checkstr(self.isN2,"yes","no","considering Nitrogen")
        self.length   =self.checkvalue("float",self.length,"domain length")
        self.ratio    =self.checkvalue("float",self.ratio,"Min. spacing ratio")
        self.slope    =self.checkvalue("float",self.slope,"Max. spacing difference")
        self.curve    =self.checkvalue("float",self.curve,"Max. slope difference")
        self.loglevel =self.checkvalue("integer",self.loglevel,"Flame solver verbosity")
        self.SS       =self.CheckRange(self.SS,"steady-state tolerance")
        self.TS       =self.CheckRange(self.TS,"time stepping tolerance")

