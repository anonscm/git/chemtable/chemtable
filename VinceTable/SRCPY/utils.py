from mpi4py import MPI
import sqlite3 as lite
from glob import glob
import configparser
import numpy as np
import h5py 
import os
from SRCPY.IO import ReadAIh5,ReadPFh5

def ConfigSectionMap(file,section):
  Config     = configparser.ConfigParser()
  Config.read(file)
  dictionary = {}
  options    = Config.options(section)
  #print options
  for option in options:
    try:
      dictionary[option]    =  Config.get(section, option)
      if dictionary[option] == -1:
        DebugPrint("skip: %s" % option)
    except:
      print("exception on %s!" % option)
      dictionary[option]    =  None
  return dictionary

def mkdir(directory):
  if not os.path.exists(directory):
    os.mkdir(directory)

def GetFiles(Root):
  h5files=[]
  patterns=['*.hdf5','*.h5']
  for p in patterns:
    p1=Root+'/'+p
    h5files.extend(glob(p1))
  return h5files

def CreateDb(file_db,Table,Point):
  con = lite.connect(file_db)
  cur = con.cursor()
  cmd="SELECT name FROM sqlite_master WHERE type='table' AND name='%s'"%Table
  output=cur.execute(cmd).fetchone()
  if (output is None):
    condition=True
  elif(Table not in output):
    condition=True
  else:
    condition=False

  if (condition):
    # keys=Point.keys()
    # values=Point.values()

    keys=[key for key in Point.keys()]
    values=[value for value in Point.values()]

    cmd='CREATE TABLE %s(Id INT' %Table
    for i in range(len(keys)):
      k=keys[i]
      t=type(values[i])
      if (t==type('')):
        tt='TEXT'
      elif((t==type(0.0)) or (t==type(0))):
        tt='REAL'
      cmd+=', '+k+' '+tt
    cmd+=')'
    cur.execute(cmd)
    con.close()

def insertDb(Point,file_db,Table,Id):
  # keys=Point.keys()
  # values=Point.values()
  keys=[key for key in Point.keys()]
  values=[value for value in Point.values()]

  cmd='INSERT INTO %s' %(Table)
  cmd+=' VALUES(%g, ' %Id
  for i in range(len(keys)):
    if (type(values[i])==type(0.0)):
      cmd+='%f, '%values[i]
    else:
      cmd+="'%s', "%values[i]
  cmd=cmd[:-2]
  cmd+=')'
  con = lite.connect(file_db)
  cur = con.cursor()   
  cur.execute(cmd)
  con.commit()
  con.close()

def BuildDB(Root,file_db,Table):
  h5files=GetFiles(Root)
  Database=[]

  for file in h5files:
    if (Table=='AI'):
      D=ReadAIh5(file)
    elif(Table=='PF'):
      D=ReadPFh5(file)
    Database.extend(D)
  
  CreateDb(file_db,Table,Database[0])
  for i in range(len(Database)):
      insertDb(Database[i],file_db,Table,i)

def diff_central(x, y):
    x0 = x[:-2]
    x1 = x[1:-1]
    x2 = x[2:]
    y0 = y[:-2]
    y1 = y[1:-1]
    y2 = y[2:]
    f = (x2 - x1)/(x2 - x0)
    return (1-f)*(y2 - y1)/(x2 - x1) + f*(y1 - y0)/(x1 - x0)

def diff(x,y):
    dummy=diff_central(x, y)
    yy=np.zeros(len(dummy)+2)
    yy[0]=(y[1]-y[0])/(x[1]-x[0])
    yy[-1]=(y[-1]-y[-2])/(x[-1]-x[-2])
    yy[1:-1]=dummy[:]
    return yy

def MpiCreate():
    comm = MPI.COMM_WORLD
    Nproc= comm.size
    mpi=False
    if (Nproc>1):
      mpi=True
    return comm,Nproc,mpi


def GetRank(comm):
    rank = comm.Get_rank()
    return rank

def BARRIER(comm):
  comm.barrier()
  
def GetChunk(comm,X):
  rank = comm.Get_rank()
  Nproc= comm.size
  if (rank==0):
    size_chunk=len(X)/Nproc
    start=np.zeros(Nproc,dtype=int)
    end=np.zeros(Nproc,dtype=int)
    for i in range(Nproc):
      start[i]=i*size_chunk
      end[i]=(i+1)*size_chunk
    
    end[-1]=np.mod(len(X),Nproc)+end[-1]

    for i in range(1,Nproc):
      data=X[start[i]:end[i]]
      comm.send(data, dest=i,tag=2)
    data=X[start[0]:end[0]]  
  else:
      data=comm.recv(source=0,tag=2)
  return data
