from SRCPY.DB import DB
from mpi4py import MPI
import sys
import os

def Main():
  comm = MPI.COMM_WORLD
  Nproc= comm.size
  rank=comm.Get_rank()
  if (rank==0):
      example='./INPUTFILES'
      directory='RESU'
      os.system('mkdir -p %s' %directory)
      cmd='cp  %s/Input.ini %s/' %(example,directory) 
      os.system(cmd)
      file_ini=directory+'/Input.ini'
      print('Program starting....')
      for i in range(1,Nproc):
        comm.send(file_ini,dest=i,tag=10)
        comm.send(directory,dest=i,tag=11)

  else:
    file_ini=comm.recv(source=0,tag=10)
    directory=comm.recv(source=0,tag=11)

  DB(file_ini,directory)

if __name__ == '__main__':
    Main()
 