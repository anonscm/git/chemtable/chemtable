import h5py
import numpy as np
import matplotlib.pyplot as plt
import sys
import ConfigParser
cfg=ConfigParser.ConfigParser()  #Sert à la lecture du fichier .ini
def transformationstringenliste(ls):
    lsp=ls.split(',')
    C=[float(lsp[0].replace('[','')),float(lsp[1].replace(']',''))]
    return C



def ReadAIh5(file,Var1,Var3,Var4,Data):
#  comp=float(file.split('/')[-1].split('.hdf5')[0].split('.h5')[0].split('-')[1])
  #if ("Yfuel" in file):
  #	type_comp='Y'
  #elif("phi" in file):
  #  type_comp='phi'
  h5f=h5py.File(file)
  X=[]

  cols1=list(h5f.keys())
  col1=('Var1-%2.3e' %Var1)
  if (col1 not in cols1):
  	print ('Temperature value %2.3e is not in keys range try with one of these ' %(Var1))
  	print (cols1)
  	sys.exit(1)

  cols2=list(h5f[col1].keys())
  col2=('Var3-%2.2f' %Var3)
  if (col2 not in cols2):
  	print ('column %s is not in columns try with one of these ' %(Var3))
  	print (cols2)
  	sys.exit(1)

  cols3=list(h5f[col1][col2].keys())
  col3=('Var4-%2.4f' %Var4)
  #print('Var4-%2.3f' %Var4)
  if (col3 not in cols3):
  	print ('column %s is not in columns try with one of these ' %(Var4))
  	print (cols3)
  	sys.exit(1)

  cols4=list(h5f[col1][col2][col3].keys())
  col4=(Data)
  if (col4 not in cols4):
  	print ('column %s is not in columns try with one of these ' %(Data))
  	print (cols4)
  	sys.exit(1)
 
  X=(h5f[col1][col2][col3][col4][...])

  return X


file='/home/colombier/Desktop/CAS6-H2-EGLANTINE/test.vince/RESU/calcul/AI/Var2-1800.000.hdf5'
#file='/home/colombier/Desktop/TEST-VINCE/Fumee Premelange/Echelle Grossiere/P et f varie T=1000K/Var1-300000.000.hdf5'

cfg.read('/home/colombier/Desktop/CAS6-H2-EGLANTINE/test.vince/INPUTFILES/Input.ini')
Var1=300000.
Var3=0.6
Delaytime= []
invDelaytime= []
Fraction= []
nvar2 =cfg.getint('Discretization','Variable 4 points Num.',)
liste=transformationstringenliste(cfg.get('Thermochemistry','Variable 4 Range'))
Var4=liste[0]
Increment4=(liste[1]-Var4)/(nvar2-1)
nvar1=5
#nvar2=100
#Var4=0.003
#Increment4=0.003

fig, ax1 = plt.subplots()
ax2 = ax1.twinx()    #Double legende verticale
deltaT=[]
for ivar1 in range(nvar1):
    for ivar2 in range(nvar2):   
      delay = ReadAIh5(file,Var1,Var3,Var4,'Ignitiontime')
      if delay>9.999999999999999e-05 and delay<9.7:
          invdelay = 1/delay if delay>9.999999999999999e-05 else 0
          Temperature=ReadAIh5(file,Var1,Var3,Var4,'Temperature')
          deltaT.append(np.amax(Temperature)-np.amin(Temperature))      
          Delaytime.append(delay)
          invDelaytime.append(invdelay)
          Fraction.append(Var4)
      Var4+=Increment4     
    ax1.plot(Fraction,Delaytime,':o',label='Dilution '+str(Var3)+' / Temperature de melange '+str(int(Temperature[0]))+'K')
    ax2.plot(Fraction,deltaT,':')    
    Delaytime=[]
    invDelaytime=[]    
    deltaT=[]
    Fraction=[]
    Var3+=0.05   
    Var4=liste[0]
ax1.set_xlabel('Fraction de melange')
ax1.set_ylabel('Delai auto-allumage (sec) (ligne continu)', color='g')

ax2.set_ylabel('Montee de Temperature (K) (ligne pointille)', color='b')
ax1.legend(loc=2)
plt.show()












