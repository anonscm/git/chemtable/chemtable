from shutil import copyfile
from SRCPY.Ai_gaz_brulee import AICompute
from SRCPY.PF import PFCompute
from SRCPY.Input import Input
from SRCPY.IO import message
from SRCPY.utils import *
import numpy as np
import ntpath


class DB():

  def __init__(self, file_ini,Root):
    self.comm,self.Nproc,self.parallel=MpiCreate()
    self.rank=GetRank(self.comm)
    self.ini   = file_ini
    self.DB_file    = None
    self.Root       = Root
    if (self.parallel):
      if (self.rank==0):
        self.input = Input(file_ini)
        self.MakeDirs()
        for i in range(1,self.Nproc):
         self.comm.send(self.input, dest=i,tag=3)
         self.comm.send(self.Results_dir, dest=i,tag=4)
     
      else:
        self.input=self.comm.recv(source=0,tag=3)
        self.Results_dir=self.comm.recv(source=0,tag=4)

      self.input.Var2=GetChunk(self.comm,self.input.Var2)

      BARRIER(self.comm)
      
    else:
      self.input = Input(file_ini)
      self.MakeDirs()


    self.DBCompute()

  def MakeDirs(self):
    self.input.Out_dir=self.Root+"/"+self.input.Out_dir
    case=self.input.Operation
    for directory in [self.input.Out_dir,self.input.Out_dir+'/Docs']: 
      mkdir (directory)
    
    mkdir(self.input.Out_dir+'/%s'%case.upper())
    self.Results_dir=self.input.Out_dir+'/%s' %case.upper()
    self.DB_file=self.input.Out_dir+'/Docs/'+'%s.db' %case.upper()
    basename=ntpath.basename(self.input.Scheme)
    copyfile(self.input.Scheme, self.input.Out_dir+"/"+basename)



  def DBCompute(self):
    Scheme       = self.input.Scheme
    fuel         = self.input.Fuel
    Var1         = self.input.Var1
    Var2         = self.input.Var2
    Var3         = self.input.Var3
    Var4         = self.input.Var4
    comp_mode    = self.input.Comp_mode
    Dir          = self.Results_dir
    LogFile      = self.input.LogFile
    condition    = self.input.BGTemp


    if (self.input.Operation=='ai'):
      dt           = self.input.AI_dt
      Time         = self.input.AI_time
      Max_Res      = self.input.Max_Res
      Max_Ite      = self.input.Max_Ite
      condition    = self.input.BGTemp
      if (condition=="hot"):
        condition='HP'
      elif(condition=='cold'):
        condition='TP'
      AICompute (Scheme,fuel,Var1,Var2,Var3,Var4,dt,Time,Dir,\
        comp_mode,res_max=Max_Res,Ite_max=Max_Ite,logfile=LogFile,condition=condition)
    
    if (self.input.Operation=='pf'):
      N2_present   = self.input.isN2
      width        = self.input.length
      loglevel     = self.input.loglevel
      SS           = self.input.SS
      TS           = self.input.TS
      ratio        = self.input.ratio
      slope        = self.input.slope
      curve        = self.input.curve
      UnSolved=PFCompute(Scheme,fuel,N2_present,comp_mode,condition,Dir,Var1,Var3,Var2,Var4,width,\
                logfile=LogFile,loglevel=loglevel,tol_ss=SS,tol_ts=TS,\
                ratio=ratio,slope=slope, curve=curve)
      
      if (self.parallel):
        BARRIER(self.comm)
        self.comm.send(UnSolved, dest=0,tag=5)
        if (self.rank==0):
          NoSol=[]
          for i in range(self.Nproc):
            s=self.comm.recv(source=i,tag=5)
            for j in range(len(s)):
              NoSol.append(s[j])
          if (len(NoSol)>0):
            msg="Non resolved points"
            message(msg,LogFile)
            for k in range(len(NoSol)):
              message(NoSol[k],LogFile)
      else:
        NoSol=UnSolved
        if (len(NoSol)>0):
          msg="Non resolved points"
          message(msg,LogFile)
          for k in range(len(NoSol)):
            message(NoSol[k],LogFile)
  
