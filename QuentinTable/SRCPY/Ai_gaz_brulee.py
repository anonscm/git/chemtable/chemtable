# -*- coding: utf-8 -*-
"""
Created on Wed May 30 16:22:27 2018

@author: colombier
"""

# coding: utf-8
from SRCPY.IO import Write_h5_AI,message
from math import log10, pow
import cantera as ct
import numpy as np
import sys
import os
def GetCompResidual(T,Teq):
    res=abs(T-Teq)/Teq
    return res


def compute(scheme,fuel,Var1,Var2,Var3,Var4,nt,dt,comp_mode,logfile,res_max=1e-3,Ite_max=1e6,condition="HP"):
    msg="Processing case with : Var1 = %14.8e , Var2= %2.2f , Var3=%2.3f , Var4=%2.4f" %(Var1,Var2,Var3,Var4)
    message(msg,logfile)

    cti = ct.Solution(scheme)
    scheme2='%s_NR.cti' %(scheme.split('.cti')[0])
    gas_a = ct.Solution(scheme2)
    gas_b = ct.Solution(scheme2)
    gas_c=ct.Solution(scheme)
    gas_d=ct.Solution(scheme2)
    gas_e=ct.Solution(scheme2)
    gasequil=ct.Solution(scheme)
    ntfix=Ite_max
    dtmax=dt*nt/ntfix
    ncalcul=int(log10(dtmax/dt)+1)
    try:
     for mm in range(ncalcul):
        Time          = []
        Temperature   = []
        Mass_Frac     = []
        Tmax          = 0.0
        derive        = 0.0
        Delaytime     = 0.0
        InDelaytime   = 0.0
        dtcalcul=dt*pow(10,mm)   
        frac2 = Var3
        frac = Var4
        gas_a.TPX = 300., ct.one_atm, 'C3H8:1'
        gas_b.TPX = 300., ct.one_atm, 'O2:0.21, N2:0.79'
        
#        gas_a.TPX = Var2, Var1, 'CH4:1., O2:2., N2:7.52'
#        gas_b.TPX = Var2, Var1, 'O2:0.21, N2:0.79 , H2O:0.15'

#        gas_a.TPX = 800.0, ct.one_atm, 'H2:2.0, O2:1.0, N2:3.76'
#        gas_b.TPX = 800.0, ct.one_atm, 'H2:0.25, O2:1.0, N2:3.76' 


# premier TEST avec nheptane : à faire.
#        gas_a.TPX = 1000.0, ct.one_atm, 'NC7H16:0.1818, O2:1.0, N2:3.76'
#        gas_b.TPX = 1000.0, ct.one_atm, 'NC7H16:0.04545, O2:1.0, N2:3.76'
#        gas_a.TPX = 1400.0, ct.one_atm, 'NC7H16:0.15, O2:1.0, N2:3.76'
#        gas_b.TPX = 1400.0, ct.one_atm, 'NC7H16:0.07, O2:1.0, N2:3.76'

        res_a = ct.Reservoir(gas_a)
        res_b = ct.Reservoir(gas_b)
        downstream = ct.Reservoir(gas_b)
        mixer = ct.IdealGasReactor(gas_b)
        mixerini=ct.IdealGasReactor(gas_b)
        mfc1 = ct.MassFlowController(res_a, mixer, mdot = frac)
        mfc2 = ct.MassFlowController(res_b, mixer, mdot = 1.0-frac)
        mfc5 = ct.MassFlowController(res_a, mixerini, mdot = 0.063)
        mfc6 = ct.MassFlowController(res_b, mixerini, mdot = 1.0-0.063)
        outlet = ct.Valve(mixer, downstream, K = 10.0)
        outletini = ct.Valve(mixerini, downstream, K = 10.0)
        simix = ct.ReactorNet([mixer])
        simixini = ct.ReactorNet([mixerini])
        t = 0.0;tini=0.0
        for n in range(30):
            tres = mixer.mass/(mfc1.mdot(t) + mfc2.mdot(t))
            tresini = mixerini.mass/(mfc5.mdot(tini) + mfc6.mdot(tini))
            t += 0.5*tres
            tini += 0.5*tresini
            simix.advance(t)
            simixini.advance(tini)
        gas_c.TPX = mixerini.T, mixerini.thermo.P, mixerini.thermo.X
        #Teq=XDil(gasequil,fuel,Var3,cd=condition)
        gas_c.equilibrate('HP', solver='vcs', rtol=1e-09, maxsteps=1000, maxiter=100, loglevel=0)
        
        gas_e.TPX=Var2, Var1, gas_c.X
 #       print(gas_c.X)
        
        gas_d.TPX = mixer.T, mixer.thermo.P, mixer.thermo.X
               
        res_e = ct.Reservoir(gas_e)
        res_d = ct.Reservoir(gas_d)
        downstream = ct.Reservoir(gas_e)
        mixer2 = ct.IdealGasReactor(gas_e)
        mfc3 = ct.MassFlowController(res_e, mixer2, mdot = frac2)
        mfc4 = ct.MassFlowController(res_d, mixer2, mdot = 1.0-frac2)
        outlet = ct.Valve(mixer2, downstream, K = 10.0)
        simix2 = ct.ReactorNet([mixer2])
        t = 0.0        
        for n in range(30):
            tres = mixer2.mass/(mfc3.mdot(t) + mfc4.mdot(t))
            t += 0.5*tres
            simix2.advance(t)
        
        gasequil.TPX = mixer2.T, mixer2.thermo.P, mixer2.thermo.X
        gasequil.equilibrate('HP', solver='vcs', rtol=1e-09, maxsteps=1000, maxiter=100, loglevel=0)
        Teq              =gasequil.T
        cti.TPX = mixer2.T, mixer2.thermo.P, mixer2.thermo.X
        r       = ct.ConstPressureReactor(cti)
        sim     = ct.ReactorNet([r])
        time    = 0.0
        for n in range(ntfix):
            time += dtcalcul
            sim.advance(time)
            Time.append(time)
            Temperature.append(r.T)
            Mass_Frac.append(r.Y)   # on obtient fraction massique par especes
            res=GetCompResidual(r.T,Teq)
#            if (res<=res_max):
#                msg='Convergence obtained within dt = %g s' %dtcalcul
#                msg2='Convergence obtained at iteration = %g' %n
#                message(msg,logfile)
#                message(msg2,logfile)
#                break
        if (res<=res_max):
            msg='Convergence obtained'
            message(msg,logfile)           
            break
        if ( ((n+1)==ntfix) & (res>res_max)) :
            msg='Case non converged.'
            message(msg,logfile)

     Time=np.array(Time)
     Temperature=np.array(Temperature)
     Mass_Frac=np.array(Mass_Frac)
     fmt="Resiual = %14.8e ,Steps taken= %%0%gd, Time=%14.8e s, Temperature = %2.2f , Teq=%2.2f" %(res,len(str(nt)),time,r.T,Teq)
     msg=fmt %(n+1)
     message(msg,logfile)
     # compute derivative
     if ((np.amax(Temperature)-np.amin(Temperature))>=50):
        derive=(Temperature[1:len(Temperature)]-Temperature[0:len(Temperature)-1])/dt
        Delaytime=Time[np.argmax(derive)]
     flag=0
    except:
     msg="Failure happend when computing case with : Var1 = %14.8e , Var2= %2.2f, Var4(%s)=%2.3f " %(Var1,Var2,comp_mode.upper(),Var4)
     message(msg,logfile)
     Time          = None
     Temperature   = None
     Mass_Frac     = None
     flag=-1     
    return Time,Temperature,Mass_Frac,Delaytime,flag
        

def AICompute(scheme,fuel,Var1,Var2,Var3,Var4,dt,Time,Dir,comp_mode,\
            res_max=1e-3,Ite_max=1e6,logfile="screen",condition='HP'):
    cti = ct.Solution(scheme)
    nt  = int(Time/dt) 
    ext="hdf5"
    if comp_mode == 'phi':
        filefmt='%s/Var2-%%2.3f.%s' %(Dir,ext)
    elif comp_mode == 'y':
        filefmt='%s/Yfuel-%%2.3f.%s' %(Dir,ext)
    if (condition=="HP"):
      dil_type="hot"
    elif (condition=="TP"):
      dil_type="cold"

    Unsolved=[]
    nvar1 = len(Var1)
    nvar2 = len(Var2)
    nvar3 = len(Var3)
    nvar4 = len(Var4)
    for ivar2 in range(nvar2):
      fileName=filefmt %Var2[ivar2]     
      Dicts    =[]
      for ivar1 in range(nvar1):
       for ivar3 in range(nvar3):
        for ivar4 in range(nvar4):
          Time,Temperature,Mass_Frac,Delaytime,flag=\
          compute(scheme,fuel,Var1[ivar1],Var2[ivar2],Var3[ivar3],Var4[ivar4],nt,dt,comp_mode,\
                  logfile,res_max=res_max,Ite_max=Ite_max,condition=condition)
          if (flag==0):
            Dict        ={}
            Dict['fuel']=fuel
            Dict['Dil_type']=dil_type
            Dict['Var1']=Var1[ivar1]
            Dict['Var3']=Var3[ivar3]
            Dict['Var4']=Var4[ivar4]
            Dict['Time']=Time
            Dict['Temperature']=Temperature
            Dict['Y']   =Mass_Frac
            Dict['tign']=Delaytime
            Dicts.append(Dict)
          else:
            D     ={}
            D['Var1']=Var1[ivar1]
            D['Var2']=Var2[ivar2]
            D['Var3']=Var3[ivar3]
            D['Var4']=Var4[ivar4]
            Unsolved.append(D)

      Write_h5_AI(fileName,cti.species_names,Dicts,logfile)
