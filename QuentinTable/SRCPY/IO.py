# coding: utf-8

from mpi4py import MPI
import h5py
import os

def message(string,logfile):
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  if (comm.size>1):
    b=" [CPU %g]" %rank
  else:
    b=''

  if (logfile=='screen'):
    print ('***%s %s'  %(b,string))
  else:
    if (os.path.exists(logfile)):
      f=open(logfile,'a')
      f.write('***%s %s\n'  %(b,string))
      f.close()
    else:
      f=open(logfile,'w')
      f.write('***%s %s\n'  %(b,string))
      f.close()      

def Write_h5_AI(fileName,names,Dicts,logfile):
  h5f   = h5py.File(fileName, 'w')
  N=len(Dicts)
  for k in range(N):
    Dict=Dicts[k]
    Var1=Dict['Var1']
    Var3=Dict['Var3']
    Var4=Dict['Var4']
    Time=Dict['Time']
    Temperature=Dict['Temperature']
    Mass_Frac=Dict['Y']   
    Delaytime=Dict['tign']
    Fuel=Dict['fuel']
    h5f.create_dataset("/Var1-%5.3e/Var3-%2.2f/Var4-%2.4f/Time"             % (Var1,Var3,Var4), data=Time)
    h5f.create_dataset("/Var1-%5.3e/Var3-%2.2f/Var4-%2.4f/Temperature"      % (Var1,Var3,Var4), data=Temperature)
    h5f.create_dataset("/Var1-%5.3e/Var3-%2.2f/Var4-%2.4f/Ignitiontime"     % (Var1,Var3,Var4), data=Delaytime)

    for i in range(len(names)):
      h5f.create_dataset("/Var1-%5.3e/Var3-%2.2f/Var4-%2.4f/%s"% (Var1,Var3,Var4,names[i]), data=Mass_Frac[:,i])    
  h5f.attrs["Fuel"]   = Fuel
  h5f.close()
  message("File written to %s" %fileName,logfile)


def Write_h5_PF(fileName,names,dict_attr,dict_data,logfile):
  h5f   = h5py.File(fileName, 'w')
  N=len(dict_data)
  for k in range(N):
    DD=dict_data[k]
    DA=dict_attr[k]
    Var3  = DA['T_init (K)']
    Var2  = DA['Var2 (Pa)']
    Var4= DA['dilution (%)']
    for D in [DD,DA]:    
      for i in range(len(D.keys())):
        key=D.keys()[i]
        value=D.values()[i]
        h5f.create_dataset("/Var3-%2.2f/Var2-%5.3e/Var4-%5.3e/%s"         % (Var3,Var2,Var4,key), data=value)
  h5f.close()
  message("File written to %s" %fileName,logfile)

def ReadAIh5(file):
  comp=float(file.split('/')[-1].split('.hdf5')[0].split('.h5')[0].split('-')[1])
  if ("Yfuel" in file):
    type_comp='Y'
  elif("phi" in file):
    type_comp='phi'

  h5f=h5py.File(file)
  items=h5f.items()
  N=len(items)
  D=[]
  for i in range(N):
    items_1=items[i][1].values()
    for j in range(len(items_1)):
      Temp=items_1[j].name.split('/')[1].split("Var3-")[-1]
      Pres=items_1[j].name.split('/')[2].split("Var2-")[-1]
      dils=items_1[j].keys()
      for k in range(len(dils)):
        Var4=dils[k].split("-")[-1]
        T_ignit=h5f[items_1[j].name+'/%s/Ignitiontime'%dils[k]][...]
        dicto={}
        dicto['filename']=file
        dicto['type_comp']=type_comp
        dicto['composition']=comp
        dicto['Temperature']=Temp
        dicto['Pressure']=Pres
        dicto['Dilution']=Var4
        dicto['Ignition_delay']=float(T_ignit)
        D.append(dicto)
  return D

# def ReadAIh5(file):
#   comp=float(file.split('/')[-1].split('.hdf5')[0].split('.h5')[0].split('-')[1])
#   if ("Yfuel" in file):
#     type_comp='Y'
#   elif("phi" in file):
#     type_comp='phi'
#   h5f=h5py.File(file)
# #  items=[item  for item in h5f.items()]
# # N=len(items)
#   D=[]
#   keys=[key for key in h5f.keys()]
#   for i in range(len(keys)):
#     Temp=float(keys[i].split("Var3-")[1])
#     keys_P=[key for key in h5f[keys[i]].keys()]
#     for j in range(len(keys_P)):
#       Pres=float(keys_P[j].split("Var2-")[1])
#       keys_D=[key for key in h5f[keys[i]][keys_P[j]].keys()]
#       for k in range(len(keys_D)):
#         Var4=float(keys_D[j].split("Var4-")[1])

#   # for i in range(N):
#   #   items_1=[value for value in items[i][1].values()]
#   #   print (items)
#   #   print (items_1)
#   #   for j in range(len(items_1)):
#   #     Temp=items_1[j].name.split('/')[1].split("Var3-")[-1]
#   #     Pres=items_1[j].name.split('/')[2].split("Var2-")[-1]
#   #     dils=items_1[j].keys()
#   #     for k in range(len(dils)):
#   #       Var4=dils[k].split("-")[-1]
#         # T_ignit=h5f[items_1[j].name+'/%s/Ignitiontime'%dils[k]][...]
#         T_ignit=h5f[keys[i]][keys_P[j]][keys_D[k]]['Ignitiontime'][...]
#         dicto={}
#         dicto['filename']=file
#         dicto['type_comp']=type_comp
#         dicto['composition']=comp
#         dicto['Temperature']=Temp
#         dicto['Pressure']=Pres
#         dicto['Dilution']=Var4
#         dicto['Ignition_delay']=float(T_ignit)
#         D.append(dicto)
#   return D

def ReadPFh5(file):
  comp=float(file.split('/')[-1].split('.hdf5')[0].split('.h5')[0].split('-')[1])
  if ("Yfuel" in file):
    type_comp='Y'
  elif("phi" in file):
    type_comp='phi'

  h5f=h5py.File(file)
  items=h5f.items()
  N=len(items)
  D=[]
  for i in range(N):
    items_1=items[i][1].values()
    for j in range(len(items_1)):
      Temp=items_1[j].name.split('/')[1].split("Var3-")[-1]
      Pres=items_1[j].name.split('/')[2].split("Var2-")[-1]
      dils=items_1[j].keys()
      for k in range(len(dils)):
        Var4=dils[k].split("-")[-1]
        SL=h5f[items_1[j].name+'/%s/SL (m per s)'%dils[k]][...]
        Tb=h5f[items_1[j].name+'/%s/Tb (K)'%dils[k]][...]
        delta=h5f[items_1[j].name+'/%s/thickness (m)'%dils[k]][...]

        dicto={}
        dicto['filename']=file
        dicto['type_comp']=type_comp
        dicto['composition']=comp
        dicto['Temperature']=Temp
        dicto['Pressure']=Pres
        dicto['Dilution']=Var4
        dicto['Laminar_speed']=float(SL)
        dicto['Flame_temperature']=float(Tb)
        dicto['Flame_thickness']=float(delta)
        D.append(dicto)
  return D