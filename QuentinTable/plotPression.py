# -*- coding: utf-8 -*-
"""
Created on Tue May 29 16:10:18 2018

@author: colombier
"""

import h5py
import numpy as np
import matplotlib.pyplot as plt
import sys


def ReadAIh5(file,Var1,Var3,Var4,Data):
#  comp=float(file.split('/')[-1].split('.hdf5')[0].split('.h5')[0].split('-')[1])
  #if ("Yfuel" in file):
  #	type_comp='Y'
  #elif("phi" in file):
  #  type_comp='phi'
  h5f=h5py.File(file)
  X=[]

  cols1=list(h5f.keys())
  col1=('Var1-%2.3e' %Var1)
  if (col1 not in cols1):
  	print ('Temperature value %2.3e is not in keys range try with one of these ' %(Var1))
  	print (cols1)
  	sys.exit(1)

  cols2=list(h5f[col1].keys())
  col2=('Var3-%2.2f' %Var3)
  if (col2 not in cols2):
  	print ('column %s is not in columns try with one of these ' %(Var3))
  	print (cols2)
  	sys.exit(1)

  cols3=list(h5f[col1][col2].keys())
  col3=('Var4-%2.4f' %Var4)
  if (col3 not in cols3):
  	print ('column %s is not in columns try with one of these ' %(Var4))
  	print (cols3)
  	sys.exit(1)

  cols4=list(h5f[col1][col2][col3].keys())
  col4=(Data)
  if (col4 not in cols4):
  	print ('column %s is not in columns try with one of these ' %(Data))
  	print (cols4)
  	sys.exit(1)
 
  X=(h5f[col1][col2][col3][col4][...])

  return X

fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
deltaT=[]
#file='/home/colombier/Desktop/CAS6-H2-EGLANTINE/test.vince/RESU/calcul/AI/Var2-1400.000.hdf5'
file='/home/colombier/Desktop/TEST-VINCE/Fumee Premelange/1300 -2000K/0.4 0.5/Var2-1500.000.hdf5'

Var1=1000000.
Var3=0.4   
Var2=1500.
Delaytime= []
invDelaytime= []
Fraction= []
Frac=[]
nvar2 =100
Var4=0.003
nvar1=6
for ivar1 in range(nvar1):
    for ivar2 in range(nvar2):   
      delay = ReadAIh5(file,Var1,Var3,Var4,'Ignitiontime')     
      if delay>9.999999999999999e-05 and delay<0.99:
          invdelay = 1/delay if delay>9.999999999999999e-05 else 0
          Delaytime.append(delay)
          invDelaytime.append(invdelay)
          Frac.append(Var4)          
          Fraction.append(Var4)
          Temperature=ReadAIh5(file,Var1,Var3,Var4,'Temperature')      
          deltaT.append(np.amax(Temperature)-np.amin(Temperature)) 
      Var4+=0.003     
    ax1.plot(Frac,Delaytime,label='Temperature '+str(int(Var2))+'K / Temperature du melange '+str(int(Temperature[0]))+'K')
    ax2.plot(Fraction,deltaT,':')      
    Delaytime=[]
    invDelaytime=[]
    deltaT=[]
    Fraction=[]
    Frac=[]
    Var2+=100.
#    file='/home/colombier/Desktop/CAS6-H2-EGLANTINE/test.vince/RESU/calcul/AI/Var2-'+str(Var2)+'00.hdf5'   
    file='/home/colombier/Desktop/TEST-VINCE/Fumee Premelange/1300 -2000K/0.4 0.5/Var2-'+str(Var2)+'00.hdf5'
    Var4=0.003
ax1.set_xlabel('Fraction de melange')
ax1.set_ylabel('Delai auto-allumage (sec) (ligne continu)', color='g')

ax2.set_ylabel('Montee de Temperature (K) (ligne pointille)', color='b')
ax1.legend(loc=1)

plt.show()
